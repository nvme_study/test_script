#!/bin/bash

cd vanilla_wo_ionice

umount -f /mnt/nvme_dir
nvme format /dev/nvme0n1
mkfs.ext4 -F /dev/nvme0n1
mount /dev/nvme0n1 /mnt/nvme_dir -t ext4

cd qd_1_random_read
./repeat_test.sh
cd ..

cd qd_4_random_read
./repeat_test.sh
cd ..

cd qd_64_random_read
./repeat_test.sh
cd ..

cd qd_1_random_write
./repeat_test.sh
cd ..

cd qd_1_seq_write
./repeat_test.sh
cd ..

cd qd_4_random_write
./repeat_test.sh
cd ..

cd qd_4_seq_write
./repeat_test.sh
cd ..

cd qd_64_random_write
./repeat_test.sh
cd ..

cd qd_64_seq_write
./repeat_test.sh
cd ..

cd qd_4_mix_random
./repeat_test.sh
cd ..

cd ..
