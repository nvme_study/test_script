#!/bin/bash

echo "***QD : $1 test start"
for inner_intr_coalesce in 0x0000
do
	echo "Start FIO Test"
    for inner_num_thread in 1 2 4 8 16 32 64 128
	do
        umount -f /mnt/nvme_dir
        nvme format /dev/nvme0n1
        mkfs.ext4 -F /dev/nvme0n1
        mount /dev/nvme0n1 /mnt/nvme_dir -t ext4
        vmstat -n 1 >> top_result_"$inner_num_thread" &
		fio --directory=/mnt/nvme_dir --name fio_test_1  --bandwidth-log --gtod_reduce=1 --randrepeat=1 \
		--ioengine=libaio --direct=1 --buffered=0 --rw=randwrite --filename=fio_test_1 --filesize=1G --numjobs=$inner_num_thread --iodepth=$1 \
		--group_reporting --norandommap > result_"$inner_intr_coalesce"_"$inner_num_thread"_115 &
		fio --directory=/mnt/nvme_dir --name fio_test_2  --bandwidth-log --gtod_reduce=1 --randrepeat=1 \
		--ioengine=libaio --direct=1 --buffered=0 --rw=randwrite --filename=fio_test_2 --filesize=1G --numjobs=$inner_num_thread --iodepth=$1 \
		--group_reporting --norandommap > result_"$inner_intr_coalesce"_"$inner_num_thread"_125 &

		WORK_PID=`jobs -l | sed '/vmstat/d' | awk '{print $2}'`
        echo $WORK_PID
		wait $WORK_PID
        killall -9 vmstat
	done
done

##### Get the Result
for inner_intr_coalesce in 0x0000
do
    for inner_num_thread in 1 2 4 8 16 32 64 128
	do
		cat result_"$inner_intr_coalesce"_"$inner_num_thread"_115 | grep runt | awk -F'[,=]' '{print $6}' >> 4core_iops_115
		cat result_"$inner_intr_coalesce"_"$inner_num_thread"_115 | grep ^"     lat (usec):" | awk -F'[=,]' '{print $6}' >> 4core_latency_115
#cat result_1C_"$inner_intr_coalesce"_"$inner_num_thread"_115 | grep runt | awk -F'[,=]' '{print $6}' >> 1core_iops_115
#cat result_1C_"$inner_intr_coalesce"_"$inner_num_thread"_115 | grep ^"     lat (usec):" | awk -F'[=,]' '{print $6}' >> 1core_latency_115
		cat result_"$inner_intr_coalesce"_"$inner_num_thread"_125 | grep runt | awk -F'[,=]' '{print $6}' >> 4core_iops_125
		cat result_"$inner_intr_coalesce"_"$inner_num_thread"_125 | grep ^"     lat (usec):" | awk -F'[=,]' '{print $6}' >> 4core_latency_125
#cat result_1C_"$inner_intr_coalesce"_"$inner_num_thread"_125 | grep runt | awk -F'[,=]' '{print $6}' >> 1core_iops_125
#cat result_1C_"$inner_intr_coalesce"_"$inner_num_thread"_125 | grep ^"     lat (usec):" | awk -F'[=,]' '{print $6}' >> 1core_latency_125
	done
done

